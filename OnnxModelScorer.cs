﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using ObjectDetection_ONNX.DataStructures;
using ObjectDetection_ONNX.YoloParser;
using static Microsoft.ML.Transforms.Image.ImageResizingEstimator;
using static Microsoft.ML.Transforms.Image.ImagePixelExtractingEstimator;

namespace ObjectDetection_ONNX
{
    class OnnxModelScorer
    {
        private readonly string imagesFolder;
        private readonly string modelLocation;
        private readonly MLContext mlContext;

        private IList<YoloBoundingBox> _boundingBoxes = new List<YoloBoundingBox>();

        public OnnxModelScorer(string imagesFolder, string modelLocation, MLContext mlContext)
        {
            this.imagesFolder = imagesFolder;
            this.modelLocation = modelLocation;
            this.mlContext = mlContext;
        }

        public struct YoloRModelSettings
        {   
            // Use network inspection tools like Netron to get input and output layer names
            public const string ModelInput = "input";
            public const string ModelOutput = "output";
        }

        static int GetDimensions(int size)
        {   // Used to resize input image to match the network dimensions
            // The input dimensions need to be a factor of 64
            decimal newsize = size / 64;
            newsize = Math.Ceiling(newsize) * 64;
            return (int)newsize;
        }

        public static int imageHeight = GetDimensions(720);
        public static int imageWidth = GetDimensions(1280);

        private ITransformer LoadModel(string modelLocation)
        {
            Console.WriteLine($"Model location: {modelLocation}");
            Console.WriteLine($"Default parameters: image size=({imageWidth}, {imageHeight})");

            var data = mlContext.Data.LoadFromEnumerable(new List<ImageNetData>());

            var pipeline = mlContext.Transforms.LoadImages(outputColumnName: "input", imageFolder: "", inputColumnName: nameof(ImageNetData.ImagePath))

                .Append(mlContext.Transforms.ResizeImages(outputColumnName: "input", imageWidth: imageWidth, imageHeight: imageHeight, inputColumnName: "input", resizing: ResizingKind.IsoPad))

                .Append(mlContext.Transforms.ExtractPixels(outputColumnName: "input", orderOfExtraction: ColorsOrder.ABGR, scaleImage: 1f / 255f, interleavePixelColors: false)

               .Append(mlContext.Transforms.ApplyOnnxModel(
                    shapeDictionary: new Dictionary<string, int[]>()
                    {
                        { "input", new[] { 1, 3, imageHeight, imageWidth } },
                    },
                    modelFile: modelLocation,
                    inputColumnNames: new[]
                    {
                        YoloRModelSettings.ModelInput
                    },
                    outputColumnNames: new[]
                    {
                        YoloRModelSettings.ModelOutput
                    }, recursionLimit: 100)));

            var model = pipeline.Fit(data);
            return model;
        }

        private IEnumerable<float[]> PredictDataUsingModel(IDataView testData, ITransformer model)
        {
            Console.WriteLine($"Images location: {imagesFolder}");
            Console.WriteLine("");
            Console.WriteLine("=====Identify the objects in the images=====");
            Console.WriteLine("");

            IDataView scoredData = model.Transform(testData);

            IEnumerable<float[]> probabilities = scoredData.GetColumn<float[]>(YoloRModelSettings.ModelOutput);

            return probabilities;
        }

        public IEnumerable<float[]> Score(IDataView data)
        {
            var model = LoadModel(modelLocation);
            return PredictDataUsingModel(data, model);
        }
    }
}
