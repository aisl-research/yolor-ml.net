﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using Microsoft.ML;
using ObjectDetection_ONNX.YoloParser;
using ObjectDetection_ONNX.DataStructures;
using System.Diagnostics;

namespace ObjectDetection_ONNX
{
    class Program
    {
        public static string GetAbsolutePath(string relativePath)
        {
            FileInfo _dataRoot = new FileInfo(typeof(Program).Assembly.Location);
            string assemblyFolderPath = _dataRoot.Directory.FullName;

            string fullPath = Path.Combine(assemblyFolderPath, relativePath);

            return fullPath;
        }

        private static void DrawBoundingBox(string inputImageLocation, string outputImageLocation, string imageName, IList<YoloBoundingBox> filteredBoundingBoxes)
        {
            Image image = Image.FromFile(Path.Combine(inputImageLocation, imageName));

            var originalImageHeight = image.Height;
            var originalImageWidth = image.Width;

            foreach (var box in filteredBoundingBoxes)
            {
                var x = (uint)Math.Max(box.Dimensions.X, 0);
                var y = (uint)Math.Max(box.Dimensions.Y, 0);

                var width = (uint)Math.Min(originalImageWidth - x, box.Dimensions.Width);
                var height = (uint)Math.Min(originalImageHeight - y, box.Dimensions.Height);

                x = (uint)originalImageWidth * x / (uint)OnnxModelScorer.imageWidth;
                y = (uint)originalImageHeight * y / (uint)OnnxModelScorer.imageHeight;

                width = (uint)originalImageWidth * width / (uint)OnnxModelScorer.imageWidth;
                height = (uint)originalImageHeight * height / (uint)OnnxModelScorer.imageHeight;

                string text = $"{box.Label} ({(box.Confidence * 100).ToString("0")}%)";
                
                using (Graphics thumbnailGraphic = Graphics.FromImage(image))
                {
                    thumbnailGraphic.CompositingQuality = CompositingQuality.HighQuality;
                    thumbnailGraphic.SmoothingMode = SmoothingMode.HighQuality;
                    thumbnailGraphic.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    // Define Text Options
                    Font drawFont = new Font("Arial", 12, FontStyle.Bold);
                    SizeF size = thumbnailGraphic.MeasureString(text, drawFont);
                    SolidBrush fontBrush = new SolidBrush(Color.Black);
                    Point atPoint = new Point((int)x, (int)y - (int)size.Height - 1);

                    // Define BoundingBox options
                    Pen pen = new Pen(box.BoxColor, 3.2f);
                    SolidBrush colorBrush = new SolidBrush(box.BoxColor);

                    thumbnailGraphic.FillRectangle(colorBrush, (int)x, (int)(y - size.Height - 1), (int)size.Width, (int)size.Height);

                    thumbnailGraphic.DrawString(text, drawFont, fontBrush, atPoint);

                    // Draw bounding box on image
                    thumbnailGraphic.DrawRectangle(pen, x, y, width, height);
                }

                if (!Directory.Exists(outputImageLocation))
                {
                    Directory.CreateDirectory(outputImageLocation);
                }

                image.Save(Path.Combine(outputImageLocation, imageName));
            }
        }

        private static void LogDetectedObjects(string imageName, IList<YoloBoundingBox> boundingBoxes)
        {
            Console.WriteLine($".....The objects in the image {imageName} are detected as below....");

            foreach (var box in boundingBoxes)
            {
                Console.WriteLine($"{box.Label} and its Confidence score: {box.Confidence}");
            }

            Console.WriteLine("");
        }

        static void Main(string[] args)
        {
            var assetsRelativePath = @"../../../assets";
            string assetsPath = GetAbsolutePath(assetsRelativePath);
            var modelFilePath = Path.Combine(assetsPath, "Model", "photoshield_1280X720-best.onnx");
            var imagesFolder = Path.Combine(assetsPath, "images");
            var outputFolder = Path.Combine(assetsPath, "images", "output");

            MLContext mlContext = new MLContext();
            var sw = new Stopwatch();
            try
            {
                IEnumerable<ImageNetData> images = ImageNetData.ReadFromFile(imagesFolder);
                sw.Start();
                IDataView imageDataView = mlContext.Data.LoadFromEnumerable(images);
                sw.Stop();
                Console.WriteLine($"[INFO] Time taken for Pre-Processing: {sw.ElapsedMilliseconds}ms.");

                var modelScorer = new OnnxModelScorer(imagesFolder, modelFilePath, mlContext);

                // Use model to score data
                sw.Start();
                IEnumerable<float[]> probabilities = modelScorer.Score(imageDataView);
                sw.Stop();

                Console.WriteLine($"[INFO] Time taken for Model predictions: {sw.ElapsedMilliseconds}ms.");

                YoloROutputParser parser = new YoloROutputParser();


                sw.Start();
                var boundingBoxes =
                    probabilities
                    .Select(probability => parser.ParseOutputs(probability))
                    .Select(boxes => parser.FilterBoundingBoxes(boxes, 5, .5F));
                
                sw.Stop();
                Console.WriteLine($"[INFO] Time taken for Post-Processing: {sw.ElapsedMilliseconds}ms.");


                for (var i = 0; i < images.Count(); i++)
                {
                    string imageFileName = images.ElementAt(i).Label;
                    
                    IList<YoloBoundingBox> detectedObjects = boundingBoxes.ElementAt(i);

                    sw.Start();
                    DrawBoundingBox(imagesFolder, outputFolder, imageFileName, detectedObjects);
                    sw.Stop();
                    
                    Console.WriteLine($"[INFO] Total Time taken: {sw.ElapsedMilliseconds}ms.");

                    LogDetectedObjects(imageFileName, detectedObjects);
                    Console.WriteLine("========= End of Process ========");
                }   
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
