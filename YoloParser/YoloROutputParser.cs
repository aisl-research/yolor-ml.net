﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace ObjectDetection_ONNX.YoloParser
{
    class CellDimensions : DimensionsBase { }

    class YoloROutputParser
    {
        private float[] anchors = new float[]
        {
            19,27,  44,40,  38,94,  96,68,  86,152,  180,137,  140,301,  303,264,  238,542,  436,615,  739,380,  925,792
        };

        private string[] labels = new string[]
        {
            "mobile"
        };

        // Colors for 20 classes - Here we have only one class
        private static Color[] classColors = new Color[]
        {
            Color.Khaki,
            Color.Fuchsia,
            Color.Silver,
            Color.RoyalBlue,
            Color.Green,
            Color.DarkOrange,
            Color.Purple,
            Color.Gold,
            Color.Red,
            Color.Aquamarine,
            Color.Lime,
            Color.AliceBlue,
            Color.Sienna,
            Color.Orchid,
            Color.Tan,
            Color.LightPink,
            Color.Yellow,
            Color.HotPink,
            Color.OliveDrab,
            Color.SandyBrown,
            Color.DarkTurquoise
        };

        // Helper functions
        private float Sigmoid(float value)
        {
            var k = (float)Math.Exp(value);
            return k / (1.0f + k);
        }

        private float[] Softmax(float[] values)
        {
            var maxVal = values.Max();
            var exp = values.Select(v => Math.Exp(v - maxVal));
            var sumExp = exp.Sum();

            return exp.Select(v => (float)(v / sumExp)).ToArray();
        }

        private ValueTuple<int, float> GetTopResult(float[] predictedClasses)
        {
            return predictedClasses
                .Select((predictedClass, index) => (Index: index, Value: predictedClass))
                .OrderByDescending(result => result.Value)
                .First();
        }

        private float IntersectionOverUnion(RectangleF boundingBoxA, RectangleF boundingBoxB)
        {
            var areaA = boundingBoxA.Width * boundingBoxA.Height;

            if (areaA <= 0)
                return 0;

            var areaB = boundingBoxB.Width * boundingBoxB.Height;

            if (areaB <= 0)
                return 0;

            var minX = Math.Max(boundingBoxA.Left, boundingBoxB.Left);
            var minY = Math.Max(boundingBoxA.Top, boundingBoxB.Top);
            var maxX = Math.Min(boundingBoxA.Right, boundingBoxB.Right);
            var maxY = Math.Min(boundingBoxA.Bottom, boundingBoxB.Bottom);

            var intersectionArea = Math.Max(maxY - minY, 0) * Math.Max(maxX - minX, 0);

            return intersectionArea / (areaA + areaB - intersectionArea);
        }        
        
        public IList<YoloBoundingBox> ParseOutputs(float[] yoloModelOutputs, float threshold = .6F)
        {
            // YOLOR outputs a tensor that has a shape of [1, no of boxes, concat_dim], which 
            // WinML flattens into a 1D array.  To access a specific channel 
            // for a given (x,y) cell position, we need to calculate an offset
            // into the array

            var sw = new Stopwatch();
            sw.Start();

            // Here the Yolor output is a tensor [1, no of boxes, concat_dim]
            // [x, y, w, h, o, c1,......] every image
            // Since we have 5 featues + 1 class the offset is 6

            int offset = 0;
            var boxes = new List<YoloBoundingBox>();

            for (int i = 0; i < yoloModelOutputs.Length / 6 - 1; i++)
            {
                float x = yoloModelOutputs[offset];
                float y = yoloModelOutputs[offset + 1];
                float w = yoloModelOutputs[offset + 2];
                float h = yoloModelOutputs[offset + 3];
                float Confidence = Sigmoid(yoloModelOutputs[offset + 4]);
                float classScore = Sigmoid(yoloModelOutputs[offset + 5]);
                
                offset += 6;

                //Confidence *= classScore;

                if (Confidence < threshold)
                    continue;

                boxes.Add(new YoloBoundingBox()
                {
                    Dimensions = new BoundingBoxDimensions
                    {
                        X = x - w/2, 
                        Y = y - h/2,
                        Width = w,
                        Height = h,
                    },

                    Confidence = Confidence,
                    Label = labels[0],
                    BoxColor = classColors[0]
                });
            }

            sw.Stop();
            Console.WriteLine($"[INFO] Time taken to ParseOutputs function {sw.ElapsedMilliseconds}ms.");
            Console.WriteLine();
            return boxes;
        }
        
        // Non maximum suppression
        public IList<YoloBoundingBox> FilterBoundingBoxes(IList<YoloBoundingBox> boxes, int limit, float threshold)
        {
            var sw = new Stopwatch();
            sw.Start();

            var activeCount = boxes.Count;
            var isActiveBoxes = new bool[boxes.Count];

            for (int i = 0; i < isActiveBoxes.Length; i++)
                isActiveBoxes[i] = true;

            var sortedBoxes = boxes.Select((b, i) => new { Box = b, Index = i })
                    .OrderByDescending(b => b.Box.Confidence)
                    .ToList();

            var results = new List<YoloBoundingBox>();

            for (int i = 0; i < boxes.Count; i++)
            {
                if (isActiveBoxes[i])
                {
                    var boxA = sortedBoxes[i].Box;
                    results.Add(boxA);

                    if (results.Count >= limit)
                        break;

                    for (var j = i + 1; j < boxes.Count; j++)
                    {
                        if (isActiveBoxes[j])
                        {
                            var boxB = sortedBoxes[j].Box;

                            if (IntersectionOverUnion(boxA.Rect, boxB.Rect) > threshold)
                            {
                                isActiveBoxes[j] = false;
                                activeCount--;

                                if (activeCount <= 0)
                                    break;
                            }
                        }
                    }
                    if (activeCount <= 0)
                        break;
                }
            }
            sw.Stop();
            Console.WriteLine($"[INFO] Time taken for NMS {sw.ElapsedMilliseconds}ms.");
            Console.WriteLine();
            return results;
        }
    }
}