﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.ML.Data;

namespace ObjectDetection_ONNX.DataStructures
{
    public class ImageNetPrediction
    {
        [ColumnName("output")]
        public float[] PredictedLabels;
    }
}
